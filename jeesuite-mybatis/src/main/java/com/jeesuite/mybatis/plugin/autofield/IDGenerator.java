package com.jeesuite.mybatis.plugin.autofield;

import java.io.Serializable;

public interface IDGenerator {

	Serializable nextId();
}
