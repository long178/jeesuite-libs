package com.jeesuite.gateway;

public class FilterConstants {

	public static final String CONTEXT_ROUTE_SERVICE = "ctx-route-svc";
	public static final String CONTEXT_IGNORE_FILTER = "ctx-ignore-filter";
	public static final String CONTEXT_TRUSTED_REQUEST = "ctx-trusted-req";
}
